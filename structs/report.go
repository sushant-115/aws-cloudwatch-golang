package structs

//Report struct for the generated reports
type Report struct {
	ServiceName  string
	ServiceID    string
	Report       string
	Utiliization string
	Timestamp    string
}
